# Spanish translation for gnome-network-displays.
# Copyright (C) 2020 gnome-network-displays's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-network-displays package.
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2020-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-network-displays master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-network-displays/"
"issues/\n"
"POT-Creation-Date: 2024-01-30 19:30+0000\n"
"PO-Revision-Date: 2024-02-28 12:27+0100\n"
"Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>\n"
"Language-Team: Spanish - Spain <gnome-es-list@gnome.org>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 45.3\n"

#: data/org.gnome.NetworkDisplays.desktop.in:3
#: data/org.gnome.NetworkDisplays.appdata.xml.in:7 src/main.c:68
msgid "GNOME Network Displays"
msgstr "Pantallas en red de GNOME"

#: data/org.gnome.NetworkDisplays.appdata.xml.in:8
msgid "Screencasting for GNOME"
msgstr "Compartición de pantalla para GNOME"

#: data/org.gnome.NetworkDisplays.appdata.xml.in:25
msgid ""
"GNOME Network Displays allows you to cast your desktop to a remote display. "
"Supports the Miracast and Chromecast protocols."
msgstr ""
"Pantallas en red de GNOME le permite mostrar su escritorio en una pantalla "
"remota. Soporta los protocolos Miracast y Chromecast."

#: src/nd-codec-install.c:59
#, c-format
msgid "GStreamer OpenH264 video encoder (%s)"
msgstr "Codificador de vídeo OpenH264 de GStreamer (%s)"

#: src/nd-codec-install.c:61
#, c-format
msgid "GStreamer x264 video encoder (%s)"
msgstr "Codificador de vídeo x264 de GStreamer (%s)"

#: src/nd-codec-install.c:63
#, c-format
msgid "GStreamer VA-API H264 video encoder (%s)"
msgstr "Codificador de vídeo VA-API H264 de GStreamer (%s)"

#: src/nd-codec-install.c:65
#, c-format
msgid "GStreamer On2 VP8 video encoder (%s)"
msgstr "Codificador de vídeo On2 VP8 de GStreamer (%s)"

#: src/nd-codec-install.c:67
#, c-format
msgid "GStreamer On2 VP9 video encoder (%s)"
msgstr "Codificador de vídeo On2 VP9 de GStreamer (%s)"

#: src/nd-codec-install.c:70
#, c-format
msgid "GStreamer FDK AAC audio encoder (%s)"
msgstr "Codificador de sonido FDK AAC de GStreamer (%s)"

#: src/nd-codec-install.c:72
#, c-format
msgid "GStreamer libav AAC audio encoder (%s)"
msgstr "Codificador de sonido libav AAC de GStreamer (%s)"

#: src/nd-codec-install.c:74
#, c-format
msgid "GStreamer Free AAC audio encoder (%s)"
msgstr "Codificador de sonido Free AAC de GStreamer (%s)"

#: src/nd-codec-install.c:76
#, c-format
msgid "GStreamer Vorbis audio encoder (%s)"
msgstr "Codificador de sonido Vorbis de GStreamer (%s)"

#: src/nd-codec-install.c:78
#, c-format
msgid "GStreamer Opus audio encoder (%s)"
msgstr "Codificador de sonido Opus de GStreamer (%s)"

#: src/nd-codec-install.c:81
#, c-format
msgid "GStreamer WebM muxer (%s)"
msgstr "Muxer WebM de GStreamer (%s)"

#: src/nd-codec-install.c:83
#, c-format
msgid "GStreamer Matroska muxer (%s)"
msgstr "Muxer Matroska de GStreamer (%s)"

#: src/nd-codec-install.c:85
#, c-format
msgid "GStreamer MPEG Transport Stream muxer (%s)"
msgstr "Muxer de flujo de transporte MPEG muxer (%s)"

#: src/nd-codec-install.c:87
#, c-format
msgid "GStreamer Element “%s”"
msgstr "Elemento «%s» de GStreamer"

#: src/nd-codec-install.c:193
msgid "Please install one of the following GStreamer plugins by clicking below"
msgstr ""
"Instale uno de los siguientes complementos de GStreamer pulsando a "
"continuación"

#: src/nd-window.c:201
msgid "Checking and installing required firewall zones."
msgstr "Comprobar e instalar zonas de cortafuegos requeridos."

#: src/nd-window.c:208
msgid "Making P2P connection"
msgstr "Crear conexiones P2P"

#: src/nd-window.c:215
msgid "Establishing connection to sink"
msgstr "Estableciendo conexión al dispositivo"

#: src/nd-window.c:222
msgid "Starting to stream"
msgstr "Empezar a emitir"

#: src/nd-window.ui:6
msgid "Network Displays"
msgstr "Pantallas en red"

#: src/nd-window.ui:28
msgid "No Wi‑Fi P2P Adapter Found"
msgstr "No se han encontrado adaptadores P2P inalámbricos"

#: src/nd-window.ui:29
msgid ""
"No usable wireless adapters were found. Please verify that Wi‑Fi is enabled "
"and Wi‑Fi P2P operations are available in both NetworkManager and "
"wpa_supplicant."
msgstr ""
"No se han encontrado adaptadores inalámbricos que poder usar. Compruebe que "
"la red inalámbrica está activada y que las operaciones P\"P inalámbricas "
"está disponibles tanto en NetworkManager como en wpa_supplicant."

#: src/nd-window.ui:63
msgid "Available Video Sinks"
msgstr "Dispositivos de vídeo disponibles"

#: src/nd-window.ui:137
msgid "Connecting"
msgstr "Conectando"

#: src/nd-window.ui:160 src/nd-window.ui:231
msgid "Cancel"
msgstr "Cancelar"

#: src/nd-window.ui:205
msgid "Streaming"
msgstr "Emitir"

#: src/nd-window.ui:276
msgid "Error"
msgstr "Error"

#: src/nd-window.ui:299
msgid "Return"
msgstr "Volver"

#: src/nd-window.ui:308
msgid ""
"One of the following codec plugins is required for audio support.\n"
"Clicking will start the installation process."
msgstr ""
"Se necesita uno de siguientes complementos del codificador de sonido.\n"
"Al pulsar se iniciará el proceso de instalación."

#: src/nd-window.ui:314
msgid ""
"One of the following codec plugins is required for video support.\n"
"Clicking will start the installation process."
msgstr ""
"Se necesita uno de siguientes complementos del codificador de vídeo.\n"
"Al pulsar se iniciará el proceso de instalación."

#: src/nd-window.ui:327
msgid ""
"A required firewall zone is not available and could not be installed. Please "
"try again and enter the password when prompted or contact a system "
"administrator."
msgstr ""
"Una zona necesaria del cortafuegos no está disponible y no se pudo instalar. "
"inténtelo de nuevo e introduzca la contraseña cuando se le pida o contacte "
"con el administrador del sistema."

#~ msgid "Stream the desktop to Wi-Fi Display capable devices"
#~ msgstr ""
#~ "Enviar el escritorio a dispositivos inalámbricos con pantalla dispoibles"

#~ msgid ""
#~ "One of the following video codec plugins is required.\n"
#~ "Clicking will start the installation process."
#~ msgstr ""
#~ "Se necesita uno de siguientes complementos del codificador de vídeo.\n"
#~ "Al pulsar se iniciará el proceso de instalación."
