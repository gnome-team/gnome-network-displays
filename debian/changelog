gnome-network-displays (0.94.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 13 Nov 2024 16:19:39 -0500

gnome-network-displays (0.93.0-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.7.0; no changes needed
  * Updated my mail address in d/control, d/copyright

 -- Matthias Geiger <werdahias@debian.org>  Wed, 14 Aug 2024 23:31:03 +0200

gnome-network-displays (0.92.2-2) unstable; urgency=medium

  * Depend on iwd or wpasupplicant (Closes: #1077980)

 -- Matthias Geiger <werdahias@debian.org>  Thu, 08 Aug 2024 11:41:31 +0200

gnome-network-displays (0.92.2-1) unstable; urgency=medium

  * New upstream release
  * Drop remaining patch: applied in new release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 29 May 2024 13:11:38 -0400

gnome-network-displays (0.92.1-2) unstable; urgency=medium

  * Add appstream to Build-Depends for build tests
  * Cherry-pick patch to allow casting a single window
  * Update package description

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 01 Feb 2024 08:31:59 -0500

gnome-network-displays (0.92.1-1) unstable; urgency=medium

  * New upstream release
    - Port to GTK4
    - Use libportal to acquire screencast portal
  * Updated my mail address in d/control and d/copyright
  * Updated copyright years
  * Updated build-dependencies for new upstream:
    - build-depend on libadwaita, libgtk4, libportal-gtk4, libglib and
      desktop-file-utils
    - versioned dependencies accordingly
    - dropped build-dependency on cmake

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 31 Jan 2024 23:21:38 +0100

gnome-network-displays (0.91.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - Add support for Miracast over Infrastructure (MICE) protocol
    - Add support for Chromecast protocol
    - Add support for casting a virtual screen
  * Build-Depends: Add libjson-glib-dev, libprotobuf-c-dev, libsoup-3.0-dev
  * Update standards version to 4.6.2, no changes needed

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 19 Jan 2024 09:14:18 -0500

gnome-network-displays (0.90.5-3) unstable; urgency=medium

  * Fixed wrong VCS fields in d/control

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Mon, 03 Oct 2022 16:13:11 -0700

gnome-network-displays (0.90.5-2) unstable; urgency=medium

  * Rebuild after NEW acceptance

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 25 Sep 2022 14:17:33 -0400

gnome-network-displays (0.90.5-1) unstable; urgency=medium

  * Initial release. (Closes: #995899)

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Thu, 01 Sep 2022 21:48:51 -0400
